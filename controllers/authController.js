const {User} = require("../models")
exports.register = (req,res)=>{
    User.register(req.body)
    .then((data)=>{
        res.redirect("/page/login")
    })
    .catch((err)=>{
        res.status(500).json({status: "Register Failed",msg: err})
    })
}

exports.login = (req,res)=>{
    User.authentication(req.body)
    .then((data)=>{
        res.json({status:"login Success", data: format(data)})
    }).catch((err)=>{
        res.status(500).json({status:"login failed",msg:err})
    })
}