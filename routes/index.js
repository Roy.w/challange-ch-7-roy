const express = require('express');
const router = express.Router();
const pageRouter = require("./page");
const authRouter = require("./auth");



/* GET home page. */


router.use("/page", pageRouter);
router.use("/auth", authRouter);



module.exports = router;




