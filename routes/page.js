const router = require("express").Router();
const page = require("../controllers/pageController");


router.get("/login",page.login);
router.get("/register",page.create);

module.exports = router

